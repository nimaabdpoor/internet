package com.nima.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley

class MainActivity : AppCompatActivity() {
    var textView : TextView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        textView = findViewById<TextView>(R.id.textv)
        val button1= findViewById<TextView>(R.id.button_one)
        val button2= findViewById<TextView>(R.id.button_second)
        val edit1= findViewById<TextView>(R.id.et_first)
        val edit2= findViewById<TextView>(R.id.et_second)
        var uri1 : String = ""
        var uri2 : String = ""
        button1.setOnClickListener {
            uri1 = edit1.text.toString()
            one(uri1)
        }
        button2.setOnClickListener {
            uri2 = edit2.text.toString()
            one(uri2)
        }

    }
     fun one(s : String):Int{
        val queue = Volley.newRequestQueue(this)
        val stringRequest = StringRequest(
            Request.Method.GET, s,
            { response ->
                // Display the first 500 characters of the response string.
                Log.d("TAG", "ddddddddddddone: ${response.toString()}")
                textView?.text = "Response is: ${response.substring(0, 20)}"
            },
            { respi ->
                textView?.text = respi.message
            })
        queue.add(stringRequest)
        queue.start()
         return 1
    }
}